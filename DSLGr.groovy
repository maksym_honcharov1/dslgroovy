
project(key: 'ASPRV2', name: 'ASP.NET TforGroovyV2') {
    plan(key: 'ASPPV2', name: 'ASP.NET GROOVY planV2') {
        description ''
        enabled true
        scm {
            github(name: 'https://github.com/maximalistich/app-service-web-dotnet-get-started.git') {
                repoSlug 'maximalistich/app-service-web-dotnet-get-started'
                branch 'master'
                passwordAuth {
                userName 'maximalistich'
                }
                advancedOptions {
                useShallowClones false
                enableRepositoryCachingOnRemoteAgents true
                useSubmodules false
                commandTimeoutInMinutes 180
                verboseLogs false
                fetchWholeRepository false
                webRepository {}
                }
            }
        }
        triggers {
               onceAday {
                    description 'run every day at noon'
                    buildTime '12:00'
               }
        }
        variables {
                variable 'Art_dest_f', 'C:/Users/Maksym_Honcharov/Bamboo/xml-data/build-dir/ASPRV2-ASPPV2-JOB2/target/aspnet-get-started.zip'
                variable 'Art_folder', 'C:/Users/Maksym_Honcharov/Bamboo/xml-data/build-dir/ASPRV2-ASPPV2-JOB2/aspnet-get-started/target/_PublishedWebsites/aspnet-get-started'
                variable 'Output_Path', 'C:/Users/Maksym_Honcharov/Bamboo/xml-data/build-dir/ASPRV2-ASPPV2-JOB2/target'
        }
        stage(name: 'Groovy Stage') {
            manual false
        
            job(key: 'DWEBAPP', name: 'DSL-Web-AppDeploy') {
                description 'deploying web-app'
                enabled true
            
                tasks { 
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.scripttask:task.builder.script') {
                        description 'WEBAPPdeploy'
                        enabled true
                        isFinal false
                        configure(
                             'argument': '-projectNamePrefix ilonkypirocet -location westeurope -stagingSlotName slotxxx -autoSwapSlotName production',
                             'scriptLocation': 'FILE',
                             'interpreter': 'POWERSHELL',
                             'script': 'C:/Users/Maksym_Honcharov/Desktop/Azuretask-6/deploywebUpp.ps1',
                        )
                    } 
                }
            } 
            job(key: 'JOB2', name: 'Main Job Build') {
                description 'MSbuild process'
                enabled true
            
                tasks { 
                    custom(pluginKey: 'com.atlassian.bamboo.plugin.dotnet:msbuild') {
                        description 'Clean build'
                        enabled false
                        isFinal false
                        configure(
                             'options': '-target:clean',
                             'label': 'latest msbuild',
                             'solution': 'aspnet-get-started.sln',
                        )
                    } 
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.vcs:task.vcs.checkout') {
                        description 'Checkout Default Repository'
                        enabled true
                        isFinal false
                        configure(
                             'cleanCheckout': 'false',
                             'selectedRepository_0': 'defaultRepository',
                        )
                    } 
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.scripttask:task.builder.command') {
                        description 'nuget'
                        enabled true
                        isFinal false
                        configure(
                             'argument': 'restore',
                             'label': 'NuGet',
                        )
                    }
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.scripttask:task.builder.script') {
                        description 'Folder'
                        enabled true
                        isFinal false
                        configure(
                            'argument': '',
                            'scriptLocation': 'INLINE',
                            'environmentVariables': '',
                            'scriptBody':'New-Item -Path "C:/Users/Maksym_Honcharov/Bamboo/xml-data/build-dir/ASPRV2-ASPPV2-JOB2/target"-ItemType Directory',
                            'interpreter': 'POWERSHELL',
                            'script': '',
                            'workingSubDirectory': '',
                        )
                    } 
                    custom(pluginKey: 'com.atlassian.bamboo.plugin.dotnet:msbuild') {
                        description 'Artifek'
                        enabled true
                        isFinal false
                        configure(
                             'options': '/p:DeployOnBuild=true /p:OutputPath=target',
                             'label': 'latest msbuild',
                             'solution': 'aspnet-get-started/aspnet-get-started.csproj',
                        )
                    } 
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.scripttask:task.builder.script') {
                        description 'Activation'
                        enabled true
                        isFinal false
                        configure(
                             'scriptLocation': 'INLINE',
                             'scriptBody': 'Compress-Archive -Path ${bamboo.Art_folder} -DestinationPath ${bamboo.Art_dest_f} -Update',
                             'interpreter': 'POWERSHELL',
                        )
                    } 
                }
            
                artifacts { 
                    definition(name: 'artifek', copyPattern: '**/*.zip') {
                        location 'target'
                        shared true
                    } 
            
                }
            } 
        } 

        deploymentProject(name: 'ASPdeployforGroovy') {
            description 'Deploying part'
        
            environment(name: 'AzureenvGroovy') {
                description 'Deploy env'
                
                variables {
                variable 'Art_dest_f_deploy', 'C:/Users/Maksym_Honcharov/Bamboo/artifacts/plan-1867777.zip'
                }
            
                tasks { 
                def Deploy_class = new GroovyScriptEngine( 'scripts/' ).with {
                    loadScriptByName( 'Deploy_class.groovy' )
                }
                this.metaClass.mixin Deploy_class
                values: [
                clean_WorkingDirectory()
                ]

                }
            } 
        } 
    }
}