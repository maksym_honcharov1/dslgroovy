 class Deploy_class {
     def clean_WorkingDirectory() {
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:cleanWorkingDirectoryTask') {
                        description 'cleanWorkingDirectoryTask'
                        enabled true
                        isFinal false
                    } 
     }

     def artifact_downloader() {
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask') {
                        description 'Download release contents'
                        enabled true
                        isFinal false
                        configure(
                             'sourcePlanKey': 'ASPR-ASPP',
                             'localPath_0': '',
                        )
                    } 
     }

     def slot_deploy() {
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.scripttask:task.builder.script') {
                        description 'slot deploy'
                        enabled true
                        isFinal false
                        configure(
                            'scriptLocation': 'FILE',
                             'interpreter': 'POWERSHELL',
                             'script': 'C:/Users/Maksym_Honcharov/Desktop/deployForDSL.ps1',
                          )
                    } 
     }
     def LocalIIS_dep() {
                    custom(pluginKey: 'com.atlassian.bamboo.plugins.scripttask:task.builder.script') {
                        description 'Local  IIS'
                        enabled true
                        isFinal false
                        configure(
                             'scriptLocation': 'FILE',
                             'interpreter': 'POWERSHELL',
                             'script': 'C:/Users/Maksym_Honcharov/Desktop/NewD.ps1',
                        )
                    }
     }
 }